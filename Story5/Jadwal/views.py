from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import JadwalForm
from .models import Jadwal
from django.utils import timezone
from datetime import datetime

# Create your views here.
def make(request):
    form = JadwalForm()
    if request.method =="POST":
        form=JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwal')
    return render(request, "form.html", {'form':form})

def jadwal(request):
    jadwals = Jadwal.objects.order_by("date")
    response = {
        'jadwal' : jadwals,    
        #'server_time' : datetime.now(),
        'time_repr' : datetime.now().strftime("%m/%d/%Y %H:%M WIB")
    }
    return render(request, "jadwal.html", response)

def remove(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('jadwal')